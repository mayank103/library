<?php

namespace App\Services;

use App\Models\Book;
use Illuminate\Support\Facades\Auth;

class BookRecommendationService
{
    public function books()
    {
        $user = Auth::user();

        if (!$user) {
            return [];
        }

        // Get the user's favorite genres
        $fav = $user->favorites()->pluck('genre')->toArray();

        // Get the books with matching genres, excluding the user's favorites
        $books = Book::whereNotIn('genre', $fav)->inRandomOrder()
                    ->take(5)
                    ->get();

        return $books;
    }
}
