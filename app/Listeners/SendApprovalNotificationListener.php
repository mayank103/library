<?php

namespace App\Listeners;

use App\Events\BookApprovalEvent;
use App\Mail\ReservationApprovalMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendApprovalNotificationListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(BookApprovalEvent $event): void
    {
        try {
            $book = $event->book;
            $username = auth()->user()->name;
            $email = auth()->user()->email;

            $event->book->increment('reserved');

            Mail::to('mayank@niswey.com')->send(new ReservationApprovalMail($book, $username, $email));
        } catch (\Throwable $th) {
            Log::error("[SendApprovalNotificationListener:handle] ".$th->getMessage());
        }
    }
}
