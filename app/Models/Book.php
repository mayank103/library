<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $hidden = ['created_at', 'updated_at'];

    public function favorites()
    {
        return $this->hasMany(Book::class, 'book_id', 'book_id');
    }

    public function reservations()
    {
        return $this->hasMany(ReserveBook::class)
            ->where('book_id', $this->id)
            ->where('user_id', auth()->user()->id);
    }
}
