<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class FavoriteController extends Controller
{

    public function favBook(Request $request)
    {
        try {
            $user_id = Auth()->user()->id;
            $book_id = $request->input('book');
            
            $attributes = [
                'user_id' => $user_id,
                'book_id' => $book_id,
            ];

            if($request->input('fav')) {
                Favorite::updateOrCreate($attributes, $attributes);
                $message = 'Favorite Book has been added!';
            } else {
                Favorite::where('user_id', $user_id)->where('book_id', $book_id)->delete();
                $message = 'Favorite Book has been removed!';
            }
            
            return redirect()->back()->with('success', $message);
        } catch (\Throwable $th) {
            Log::info('[FavoriteController::favBook] Unable to create Favorite! '.$th->getMessage());
        }        
    }
}
