<?php

namespace App\Http\Controllers;

use App\Events\BookApprovalEvent;
use App\Jobs\SendReservationApprovalEmailJob;
use App\Models\Book;
use App\Models\ReserveBook;
use App\Services\BookRecommendationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BookController extends Controller
{
    protected $recommend;

    public function __construct(BookRecommendationService $recommend)
    {
        $this->recommend = $recommend;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try {
            $keyword = '';
            $books = Book::paginate(10);
            $recommendBooks = $this->recommend->books();
            return view('welcome', compact('books', 'recommendBooks', 'keyword'));
        } catch (\Throwable $th) {
           Log::error('[BookController:index] '. $th->getMessage());
        }
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reserve(Book $book)
    {
        try{
            $user = auth()->user();
            if ($user->reservedBooks()->where('book_id', $book->id)->exists()) {
                session()->flash('error', 'You have already reserved this book.');
                return redirect()->back();
            }

            $reservation = new ReserveBook();
            $reservation->book_id = $book->id;
            $reservation->user_id = $user->id;
            $reservation->save();

            $book = $reservation->book;
            $username = $reservation->user->name;
            $email = $reservation->user->email;

            event(new BookApprovalEvent($book));

            SendReservationApprovalEmailJob::dispatch($book, $username, $email)->onQueue('emails');
            session()->flash('success', 'A request has been generated to reserve this book');

            return redirect()->back();
        } catch (\Throwable $th) {
            Log::error('[BookController:reserve] Error reserving book: '.$th->getMessage());
            return redirect()->back()->with('error', 'Error reserving book');
        }
    }

    public function search(Request $request)
    {
        try {
            $recommendBooks = $this->recommend->books();
            $keyword = $request->input('keyword');

            $books = Book::where('name', 'like', "%{$keyword}%")
                    ->orWhere('author', 'like', "%{$keyword}%")
                    ->orWhere('genre', 'like', "%{$keyword}%")
                    ->orWhere('language', 'like', "%{$keyword}%")
                    ->orWhere('year', 'like', "%{$keyword}%")
                    ->paginate(10);

            return view('welcome', compact('books', 'keyword', 'recommendBooks'));
        } catch (\Throwable $th) {
            Log::error('[BookController:index]', $th->getMessage());
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function deleteBook(Book $book)
    {
        try {
            // Delete the book
            $book->delete();
            return redirect()->back()->with('success', 'Book deleted successfully!');
        } catch (\Throwable $th) {
            Log::error('[BookController:deleteBook] Error deleting book: '.$th->getMessage());
            return redirect()->back()->with('error', 'Error deleting book');
        }
    }
}
