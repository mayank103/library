<?php

namespace App\Jobs;

use App\Mail\ReservationApprovalMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendReservationApprovalEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $book, $username, $email;

    /**
     * Create a new job instance.
     */
    public function __construct($book, $username, $email)
    {
        $this->book = $book;
        $this->username = $username;
        $this->email = $email;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $adminEmail = 'mayank@niswey.com';
        Mail::to($adminEmail)->send(new ReservationApprovalMail($this->book, $this->username, $this->email));
    }
}
