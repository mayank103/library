@extends('layouts.app')

@section('content')
<div class="container">
    @if(auth()->user())
        @if(auth()->user()->email_verified_at == null)
            <p>
                <span class="alert-danger">Please authenticate your email by checking your inbox!</span>
            </p>
        @endif
    @endif

    @if(session('success'))
        <span class="alert-success">{{session('success')}}</span>
    @endif
    <h2>Book List</h2>
    <form action="{{ route('books.search') }}" method="GET">
        <div class="form-group col-md-4">
            <input type="text" class="form-control" name="keyword" placeholder="Search here" value="{{ $keyword }}">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
    </form>
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Author</th>
                <th>Year</th>
                <th>Genre</th>
                <th>Language</th>
                @if(auth()->user())
                    @if(auth()->user()->is_admin !== 1)
                        <th>Reserve</th>
                    @endif
                    <th>Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($books as $book)
                <tr>
                    <td>{{ $book->name }}</td>
                    <td>{{ $book->author }}</td>
                    <td>{{ $book->year }}</td>
                    <td>{{ $book->genre }}</td>
                    <td>{{ $book->language }}</td>
                    @if(auth()->user() && auth()->user()->is_admin !== 1)
                        <td>
                            @php
                                $reservation = $book->reservations->where('user_id', auth()->user()->id)->first();
                            @endphp

                            @if($reservation)
                                @if($reservation->approval == 1)
                                    <p class="">Approved</p>
                                @else
                                    <p>Waiting for approval</p>
                                @endif
                            @else
                                <form action="{{ route('books.reserve', $book->id) }}" method="POST">
                                    @csrf
                                    <button type="submit">Reserve</button>
                                </form>
                            @endif
                        </td>
                    @endif
                        @if(auth()->user())
                        <td>
                            @if(auth()->user()->is_admin == 1)
                                <form action="{{ route('books.delete', $book->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this book?')">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit">Delete</button>
                                </form>
                            @else
                                <form action="{{ route('books.favorites') }}" method="POST">
                                    @csrf
                                    @if (auth()->user()->favorites->contains($book))
                                        <input type="hidden" name="fav" value="0">
                                        <input type="hidden" name="book" value="{{$book->id}}">
                                    @else
                                        <input type="hidden" name="fav" value="1">
                                        <input type="hidden" name="book" value="{{$book->id}}">
                                    @endif
                                    <button type="submit" style="background-color: transparent; border: none;">
                                        <i class="fa fa-star" @if (auth()->user()->favorites->contains($book)) style="color: gold; @else style="color: grey; @endif"></i>
                                    </button>
                                </form>
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
            @if($books->isEmpty())
                <tr>
                    <td colspan="6">No results found.</td>
                </tr>
            @endif
        </tbody>
    </table>
    <div class="d-flex">
        {!! $books->links() !!}
    </div>
</div>
<div class="container">
    <h2>Recommended Books</h2>
    <div class="list-group">
        @foreach($recommendBooks as $rcBook)
            <a href="#" class="list-group-item list-group-item-action">{{ $rcBook->name }}</a>
        @endforeach
    </div>
</div>

@endsection