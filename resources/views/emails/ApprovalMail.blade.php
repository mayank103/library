<x-mail::message>
# Book Approval Request

Dear Admin

A book approval request has been received. Please review the details below:

Book Details
# Title : {{ $book->name }}<br>
# Author : {{ $book->author }}<br>
# Year : {{ $book->year }}<br>
# Genre : {{ $book->genre }}<br>
# Language : {{ $book->language }}<br>

User Details
# Username # {{ $username }}
# Email # {{ $email }}

Please review the book

<x-mail::button :url="''">
Approve Book
</x-mail::button>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
