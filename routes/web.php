<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [App\Http\Controllers\BookController::class, 'index'])->name('home');

Route::group(['prefix' => 'books', 'middleware' => 'admin'], function () {
    Route::delete('delete/{book}', [App\Http\Controllers\BookController::class, 'deleteBook'])->name('books.delete');
});

Route::group(['prefix' => 'books', 'middleware' => 'auth'], function () {
    Route::post('favorite', [App\Http\Controllers\FavoriteController::class, 'favBook'])->name('books.favorites');
});

Route::post('books/reserve/{book}', [App\Http\Controllers\BookController::class, 'reserve'])->name('books.reserve');
Route::get('/books/search', [App\Http\Controllers\BookController::class, 'search'])->name('books.search');

Route::get('import', function() {
    $data = json_decode(file_get_contents(public_path('Books.json')));
    foreach ($data as $key => $value) {
        // CONVERT DATE FORMAT
        $value->name = $value->name;
        $value->author = $value->author;
        $value->year = $value->year;
        $value->genre = $value->genre;
        $value->language = $value->language;
        $value->created_at = Carbon::now()->format('Y-m-d H:i:s');
        $value->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        // INSERT DATA
        DB::table('books')->insert((array)$value);
    }

    return 'done';
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

